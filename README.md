# Wordpress en Raspberry Pi 4 (ARM64)

El container certbot está deshabilitado porque en mi caso los certificados los genero por medio de [acme.sh](https://github.com/acmesh-official/acme.sh).


Es necesario cambiar el dominio en [nginx.conf](nigx-conf/nginx.conf).

Es altamente recomendable cambiar las contraseñas en .env[.env](.env).

Si se usa un proxy reverso también hay que cambiar el dominio en la variable VIRTUAL_HOST en [docker_compose.yml](docker_compose.yml).
